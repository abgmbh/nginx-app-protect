output "NGINX-pip" {
  value = azurerm_public_ip.ext-1-pip.ip_address
}

output "Ubuntu-pip" {
  value = azurerm_public_ip.ext-2-pip.ip_address
}