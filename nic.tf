resource "azurerm_network_interface" "ext-1-nic" {
  name                = "${var.prefix}-ext-1-nic"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ext.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ext-1-ip
    primary                       = true
    public_ip_address_id          = azurerm_public_ip.ext-1-pip.id
  }
}

resource "azurerm_network_interface" "ext-2-nic" {
  name                = "${var.prefix}-ext-2-nic"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ext.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ext-2-ip
    primary                       = true
    public_ip_address_id          = azurerm_public_ip.ext-2-pip.id
  }
}

resource "azurerm_network_interface_security_group_association" "ext-1-nsg-ass" {
  network_interface_id      = azurerm_network_interface.ext-1-nic.id
  network_security_group_id = azurerm_network_security_group.ext-nsg.id
}

resource "azurerm_network_interface_security_group_association" "ext-2-nsg-ass" {
  network_interface_id      = azurerm_network_interface.ext-2-nic.id
  network_security_group_id = azurerm_network_security_group.ext-nsg.id
}