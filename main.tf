# Terraform Version Pinning
/*
terraform {
  required_version = "~> 0.12.26"
  required_providers {
    azurerm = "~> 2.1.0"
  }
}
*/

# Azure Provider
provider "azurerm" {
  features {}
  client_id                   = var.azure_client_id
  client_certificate_path     = var.client_certificate_path
  client_certificate_password = var.client_certificate_password
  subscription_id             = var.azure_subscription_id
  tenant_id                   = var.azure_tenant_id
}

resource "random_id" "id" {
  byte_length = 2
}

# Create a Resource Group
resource "azurerm_resource_group" "main" {
  name     = format("%s-rg-%s", var.prefix, random_id.id.hex)
  location = var.location
}
