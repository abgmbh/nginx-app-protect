# Azure Environment
variable "azure_client_id" {}
variable "client_certificate_path" {}
variable "client_certificate_password" {}
variable "azure_subscription_id" {}
variable "azure_tenant_id" {}
variable "prefix" { default = "cz-nap" }
variable "location" { default = "australiasoutheast" }
variable "me" {}

# NETWORK
variable "cidr" { default = "10.90.0.0/16" }
variable "subnets" {
  type = map(any)
  default = {
    "ext" = "10.90.1.0/24"
  }
}
variable "ext-gw" { default = "10.90.1.1" }
variable "ext-1-ip" { default = "10.90.1.4" }
variable "ext-2-ip" { default = "10.90.1.5" }

# VM
variable "instance-type-1" { default = "Standard_F4s_v2" }
variable "instance-type-2" { default = "Standard_F8s_v2" }
variable "username" { default = "ubuntu" }
