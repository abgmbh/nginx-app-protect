resource "azurerm_linux_virtual_machine" "NGINX-App-Protect-vm" {
  name                            = "${var.prefix}-NGINX-App-Protect-vm"
  location                        = azurerm_resource_group.main.location
  resource_group_name             = azurerm_resource_group.main.name
  network_interface_ids           = [azurerm_network_interface.ext-1-nic.id]
  size                            = var.instance-type-1
  admin_username                  = var.username
  disable_password_authentication = true
  computer_name                   = "${var.prefix}-NGINX-App-Protect-vm"

  os_disk {
    name                 = "${var.prefix}-NGINX-App-Protect-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}

resource "azurerm_linux_virtual_machine" "NGINX-Controller-vm" {
  name                            = "${var.prefix}-NGINX-Controller-vm"
  location                        = azurerm_resource_group.main.location
  resource_group_name             = azurerm_resource_group.main.name
  network_interface_ids           = [azurerm_network_interface.ext-2-nic.id]
  size                            = var.instance-type-2
  admin_username                  = var.username
  disable_password_authentication = true
  computer_name                   = "nginxcontroller"

  os_disk {
    name                 = "${var.prefix}-NGINX-Controller-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    disk_size_gb         = "100"
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}
